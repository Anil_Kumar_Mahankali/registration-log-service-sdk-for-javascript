/**
 * @class {RegistrationLogSynopsisView}
 */
export default class RegistrationLogSynopsisView{

    _registrationId:number;

    _partnerSaleRegistrationId:number;

    _accountId:string;

    _sellDate:string;

    _installDate:string;

    _submittedDate:string;

    _facilityName:string;

    _extendedWarrantyStatus:string;

    _spiffStatus:string;

    constructor(
        registrationId:number,
        partnerSaleRegistrationId:number,
        accountId:string,
        sellDate:string,
        installDate:string,
        submittedDate:string,
        facilityName:string,
        extendedWarrantyStatus:string,
        spiffStatus:string
    ){

        if(!registrationId){
            throw new TypeError('registrationId required');
        }
        this._registrationId = registrationId;

        if(!partnerSaleRegistrationId){
            throw new TypeError('partnerSaleRegistrationId required');
        }
        this._partnerSaleRegistrationId = partnerSaleRegistrationId;

        if(!accountId){
            throw new TypeError('accountId required');
        }
        this._accountId = accountId;

        if(!sellDate){
            throw new TypeError('sellDate required');
        }
        this._sellDate = sellDate;

        this._installDate = installDate;

        if(!submittedDate){
            throw new TypeError('submittedDate required');
        }
        this._submittedDate = submittedDate;

        if(!facilityName){
            throw new TypeError('facilityName required');
        }
        this._facilityName = facilityName;

        if(!extendedWarrantyStatus){
            throw new TypeError('extendedWarrantyStatus required');
        }
        this._extendedWarrantyStatus = extendedWarrantyStatus;

        if(!spiffStatus){
            throw new TypeError('spiffStatus required');
        }
        this._spiffStatus = spiffStatus;

    }

    /**
     * getter methods
     */
    get registrationId():number{
        return this._registrationId;
    }

    /**
     * @returns {number}
     */
    get partnerSaleRegistrationId():number{
        return this._partnerSaleRegistrationId;
    }

    /**
     * @returns {string}
     */
    get accountId():string{
        return this._accountId;
    }

    /**
     * @returns {string}
     */
    get sellDate():string{
        return this._sellDate;
    }

    /**
     * @returns {string}
     */
    get installDate():string{
        return this._installDate;
    }

    /**
     * @returns {string}
     */
    get submittedDate():string{
        return this._submittedDate;
    }

    /**
     * @returns {string}
     */
    get facilityName():string {
        return this._facilityName;
    }

    /**
     * @returns {string}
     */
    get extendedWarrantyStatus():string {
        return this._extendedWarrantyStatus;
    }

    /**
     * @returns {string}
     */
    get spiffStatus():string {
        return this._spiffStatus;
    }


    toJSON() {
        return {
            registrationId:this._registrationId,
            partnerSaleRegistrationId: this._partnerSaleRegistrationId,
            accountId: this._accountId,
            sellDate: this._sellDate,
            installDate: this._installDate,
            submittedDate: this._submittedDate,
            facilityName: this._facilityName,
            extendedWarrantyStatus: this._extendedWarrantyStatus,
            spiffStatus: this._spiffStatus
        }
    }

}

