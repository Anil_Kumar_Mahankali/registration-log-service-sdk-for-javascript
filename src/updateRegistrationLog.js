/**
 * @class {UpdateRegistrationLog}
 */
export default class UpdateRegistrationLog{

    _partnerSaleRegistrationId:number;

    _spiffStatus:string;

    /**
     * @param partnerSaleRegistrationId
     * @param spiffStatus
     */
    constructor(partnerSaleRegistrationId:number,
                spiffStatus:string
                ) {

        if(!partnerSaleRegistrationId){
            throw new TypeError('partnerSaleRegistrationId required');
        }
        this._partnerSaleRegistrationId = partnerSaleRegistrationId;

        if(!spiffStatus){
            throw new TypeError('spiffStatus required');
        }
        this._spiffStatus = spiffStatus;
    }

    /**
     * @returns {string}
     */
    get PartnerSaleRegistration():number {
        return this._partnerSaleRegistrationId;
    }

    /**
     * @returns {string}
     */
    get spiffStatus():string {
        return this._spiffStatus;
    }

    toJSON() {
        return {
            partnerSaleRegistrationId : this._partnerSaleRegistrationId,
            spiffStatus : this._spiffStatus
        }
    }
}