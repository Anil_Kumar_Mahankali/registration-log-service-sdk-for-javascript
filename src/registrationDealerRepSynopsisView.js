/**
 * @class {RegistrationDealerRepSynopsisView}
 */
export default class RegistrationDealerRepSynopsisView{

    _registrationId:number;

    _firstName:string;

    _lastName:string;

    constructor(
        registrationId:number,
        firstName:string,
        lastName:string
    ){

        this._registrationId = registrationId;

        this._firstName = firstName;

        this._lastName = lastName;

    }

    /**
     * getter methods
     */
    get registrationId():number{
        return this._registrationId;
    }

    /**
     * @returns {string}
     */
    get firstName():string{
        return this._firstName;
    }

    /**
     * @returns {string}
     */
    get lastName():string{
        return this._lastName;
    }

    toJSON() {
        return {
            registrationId:this._registrationId,
            firstName:this._firstName,
            lastName: this._lastName
        }
    }

}

