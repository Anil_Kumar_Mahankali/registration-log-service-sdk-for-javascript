import {inject} from 'aurelia-dependency-injection';
import RegistrationLogServiceSdkConfig from './registrationLogServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import RegistrationLogSynopsisView from './registrationLogSynopsisView';

@inject(RegistrationLogServiceSdkConfig, HttpClient)
class ListRegistrationLogFeature {

    _config:RegistrationLogServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:RegistrationLogServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Lists Registration Log by accountId
     * @param {string} accountId
     * @param {string} accessToken
     * @returns {Promise.<RegistrationLogSynopsisView>}
     */
    execute(accountId:string,accessToken:string):Promise<RegistrationLogSynopsisView> {

        return this._httpClient
            .createRequest(`registration-log/${accountId}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response => {
                return Array.from(
                    response.content,
                    contentItem =>
                        new RegistrationLogSynopsisView(
                            contentItem.registrationId,
                            contentItem.partnerSaleRegistrationId,
                            contentItem.accountId,
                            contentItem.sellDate,
                            contentItem.installDate,
                            contentItem.submittedDate,
                            contentItem.facilityName,
                            contentItem.extendedWarrantyStatus,
                            contentItem.spiffStatus
                        )
                );
            });
    }
}

export default ListRegistrationLogFeature;
