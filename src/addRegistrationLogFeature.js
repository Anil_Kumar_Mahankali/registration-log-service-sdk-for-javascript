import {inject} from 'aurelia-dependency-injection';
import RegistrationLogServiceSdkConfig from './registrationLogServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import AddPartnerSaleRegistrationLog from './addPartnerSaleRegistrationLog';

@inject(RegistrationLogServiceSdkConfig, HttpClient)
class AddRegistrationLogFeature {

    _config:RegistrationLogServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:RegistrationLogServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Adds Registration Log
     * @param {RegistrationLogServiceSdkConfig} request
     * @param {string} accessToken
     * @returns {Promise.<number>} LogId
     */
    execute(request:AddPartnerSaleRegistrationLog, accessToken:string):Promise<number> {

        return this._httpClient
            .createRequest('registration-log/addRegistrationLog')
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response.content);
    }
}

export default AddRegistrationLogFeature;