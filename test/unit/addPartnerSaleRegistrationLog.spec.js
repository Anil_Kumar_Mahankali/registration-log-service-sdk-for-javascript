import AddPartnerSaleRegistrationLog from '../../src/addPartnerSaleRegistrationLog';
import dummy from '../dummy';

/*
 tests
 */
describe('AddPartnerSaleRegistrationLog class', () => {
    describe('constructor', () => {
        it('throws if partnerSaleRegistrationId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddPartnerSaleRegistrationLog(
                        null,
                        dummy.partnerAccountId,
                        dummy.accountName,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.submittedDate,
                        dummy.extendedWarrantyStatus
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'partnerSaleRegistrationId required');

        });
        it('sets partnerSaleRegistrationId', () => {
            /*
             arrange
             */
            const expectedPartnerSaleRegistrationId = dummy.partnerSaleRegistrationId;

            /*
             act
             */
            const objectUnderTest =
                new AddPartnerSaleRegistrationLog(
                    expectedPartnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.accountName,
                    dummy.sellDate,
                    dummy.installDate,
                    dummy.submittedDate,
                    dummy.extendedWarrantyStatus
                );

            /*
             assert
             */
            const actualPartnerSaleRegistrationId =
                objectUnderTest.partnerSaleRegistrationId;

            expect(actualPartnerSaleRegistrationId ).toEqual(expectedPartnerSaleRegistrationId);

        });
        it('throws if partnerAccountId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                new AddPartnerSaleRegistrationLog(
                    dummy.partnerSaleRegistrationId,
                    null,
                    dummy.accountName,
                    dummy.sellDate,
                    dummy.installDate,
                    dummy.submittedDate,
                    dummy.extendedWarrantyStatus
                );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'partnerAccountId required');

        });
        it('sets partnerAccountId', () => {
            /*
             arrange
             */
            const expectedPartnerAccountId = dummy.partnerAccountId;

            /*
             act
             */
            const objectUnderTest =
                new AddPartnerSaleRegistrationLog(
                    dummy.partnerSaleRegistrationId,
                    expectedPartnerAccountId,
                    dummy.accountName,
                    dummy.sellDate,
                    dummy.installDate,
                    dummy.submittedDate,
                    dummy.extendedWarrantyStatus
                );

            /*
             assert
             */
            const actualPartnerAccountId =
                objectUnderTest.partnerAccountId;

            expect(actualPartnerAccountId).toEqual(expectedPartnerAccountId);

        });

        it('throws if accountName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddPartnerSaleRegistrationLog(
                        dummy.partnerSaleRegistrationId,
                        dummy.partnerAccountId,
                        null,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.submittedDate,
                        dummy.extendedWarrantyStatus
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'accountName required');

        });
        it('sets accountName', () => {
            /*
             arrange
             */
            const expectedAccountName = dummy.accountName;

            /*
             act
             */
            const objectUnderTest =
                new AddPartnerSaleRegistrationLog(
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    expectedAccountName,
                    dummy.sellDate,
                    dummy.installDate,
                    dummy.submittedDate,
                    dummy.extendedWarrantyStatus
                );

            /*
             assert
             */
            const actualAccountName =
                objectUnderTest.accountName;

            expect(actualAccountName).toEqual(expectedAccountName);

        });

        it('throws if sellDate is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddPartnerSaleRegistrationLog(
                        dummy.partnerSaleRegistrationId,
                        dummy.partnerAccountId,
                        dummy.accountName,
                        null,
                        dummy.installDate,
                        dummy.submittedDate,
                        dummy.extendedWarrantyStatus
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError,'sellDate required');
        });
        it('sets sellDate',() => {
            /**
             * arrange
             */
            const expectedSellDate = dummy.sellDate;

            /**
             * act
             */
            const objectUnderTest =
                new AddPartnerSaleRegistrationLog(
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.accountName,
                    expectedSellDate,
                    dummy.installDate,
                    dummy.submittedDate,
                    dummy.extendedWarrantyStatus
                );


            /**
             assert
             */
            const actualSellDate
                = objectUnderTest.sellDate;

            expect(actualSellDate).toEqual(expectedSellDate);
        });
        it('does not throw if installDate is null',() => {
            /**
             arrange
             */
            const constructor = () =>
                new AddPartnerSaleRegistrationLog(
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.accountName,
                    dummy.sellDate,
                    null,
                    dummy.submittedDate,
                    dummy.extendedWarrantyStatus
                );
            /**
             * act/assert
             */
                //expect(constructor).toThrowError(TypeError,'customerBrand required');
            expect(constructor).not.toThrow()
        });
        it('sets installDate',()=>{
            /**
             arrange
             */
            const expectedInstallDate = dummy.installDate;

            /**
             * act
             */
            const objectUnderTest =
                new AddPartnerSaleRegistrationLog(
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.accountName,
                    dummy.sellDate,
                    expectedInstallDate,
                    dummy.submittedDate,
                    dummy.extendedWarrantyStatus
                );

            /**
             * act
             */
            const actualInstallDate
                = objectUnderTest.installDate;

            expect(actualInstallDate).toEqual(expectedInstallDate);
        });
        it('throws if submittedDate is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddPartnerSaleRegistrationLog(
                        dummy.partnerSaleRegistrationId,
                        dummy.partnerAccountId,
                        dummy.accountName,
                        dummy.sellDate,
                        dummy.installDate,
                        null,
                        dummy.extendedWarrantyStatus
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError,'submittedDate required');
        });
        it('sets submittedDate',() => {
            /**
             * arrange
             */
            const expectedSubmittedDate = dummy.submittedDate;

            /**
             * act
             */
            const objectUnderTest =
                new AddPartnerSaleRegistrationLog(
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.accountName,
                    dummy.sellDate,
                    dummy.installDate,
                    expectedSubmittedDate,
                    dummy.extendedWarrantyStatus
                );


            /**
             assert
             */
            const actualSubmittedDate
                = objectUnderTest.submittedDate;

            expect(actualSubmittedDate).toEqual(expectedSubmittedDate);
        });
        it('throws if extendedWarrantyStatus is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddPartnerSaleRegistrationLog(
                        dummy.partnerSaleRegistrationId,
                        dummy.partnerAccountId,
                        dummy.accountName,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.submittedDate,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError,'extendedWarrantyStatus required');
        });
        it('sets extendedWarrantyStatus',() => {
            /**
             * arrange
             */
            const expectedExtendedWarrantyStatus = dummy.extendedWarrantyStatus;

            /**
             * act
             */
            const objectUnderTest =
                new AddPartnerSaleRegistrationLog(
                    dummy.partnerSaleRegistrationId,
                    dummy.partnerAccountId,
                    dummy.accountName,
                    dummy.sellDate,
                    dummy.installDate,
                    dummy.submittedDate,
                    expectedExtendedWarrantyStatus
                );


            /**
             assert
             */
            const actualExtendedWarrantyStatus
                = objectUnderTest.extendedWarrantyStatus;

            expect(actualExtendedWarrantyStatus).toEqual(expectedExtendedWarrantyStatus);
        });
    });
});
