import RegistrationLogServiceSdk,
{AddPartnerSaleRegistrationLog,
    RegistrationLogSynopsisView,
    UpdateRegistrationLog}
    from '../../src/index';

import config from './config';
import factory from './factory';
import dummy from '../dummy';

describe('Index module', () => {

    describe('default export', () => {
        it('should be RegistrationLogServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new RegistrationLogServiceSdk(config.registrationLogServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(RegistrationLogServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('listRegistrationLogWithId method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new RegistrationLogServiceSdk(config.registrationLogServiceSdkConfig);

                /*
                 act
                 */

                const actPromise =
                    objectUnderTest
                       .listRegistrationLogWithId(
                           dummy.partnerAccountId,
                           factory
                               .constructValidAppAccessToken()
                       );

                /*
                 assert
                 */
                actPromise
                    .then((RegistrationLogSynopsisView) => {
                        expect(RegistrationLogSynopsisView).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });

        describe('listDealerRepFirstAndLastName method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new RegistrationLogServiceSdk(config.registrationLogServiceSdkConfig);

                /*
                 act
                 */

                const actPromise =
                    objectUnderTest
                        .listDealerRepFirstAndLastName(
                            [dummy.partnerSaleRegistrationId],
                            factory
                                .constructValidAppAccessToken()
                        );

                /*
                 assert
                 */
                actPromise
                    .then((result) => {
                        expect(result).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });

    });
});