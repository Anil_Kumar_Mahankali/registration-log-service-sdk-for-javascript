import config from './config';
import jwt from 'jwt-simple';
import dummy from '../dummy';
import AddPartnerSaleRegistrationLog from '../../src/addPartnerSaleRegistrationLog';

export default {
    constructValidAppAccessToken,
    constructValidPartnerRepOAuth2AccessToken,
    constructValidAddRegistrationLogRequest
}

function constructValidAppAccessToken():string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": "app",
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}

function constructValidAddRegistrationLogRequest():AddPartnerSaleRegistrationLog {

    return new AddPartnerSaleRegistrationLog(
        dummy.partnerSaleRegistrationId,
        dummy.partnerAccountId,
        dummy.accountName,
        dummy.sellDate,
        dummy.installDate,
        dummy.submittedDate
    );
}


function constructValidPartnerRepOAuth2AccessToken(accountId:string = dummy.accountId):string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": 'partnerRep',
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url,
        "given_name": dummy.firstName,
        "family_name": dummy.lastName,
        "sub": dummy.userId,
        "account_id": accountId,
        "sap_vendor_number": dummy.sap_vendor_number
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}


