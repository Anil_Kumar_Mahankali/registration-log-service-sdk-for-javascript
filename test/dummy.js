/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */


const dummy = {
    firstName: 'firstName',
    lastName: 'lastName',
    userId: 'fake-email@test.com',
    registrationId:1,
    partnerSaleRegistrationId: 1280,
    partnerAccountId:"001K000001H2Km2IAF",
    accountName : "DummyAccountName",
    sellDate:'10/5/2016',
    installDate:'11/5/2016',
    submittedDate: '12/5/2016',
    spiffStatus : "Not Submitted",
    facilityName: "DummyFacilityName",
    extendedWarrantyStatus : "Not Submitted",
    url: 'https://test-url.com',
    sapVendorNumber: '0000000000'
};


export default dummy;



